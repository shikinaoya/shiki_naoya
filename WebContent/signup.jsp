<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- BootstrapのCSS読み込み -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- jQuery読み込み -->
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <!-- BootstrapのJS読み込み -->
    <script src="js/bootstrap.min.js"></script>
    <title>ユーザー登録</title>
    <link href="./css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="main-contents">
            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session" />
            </c:if>
            <div class="header">
		        <ol class="breadcrumb">
			        <li class="active"><a href="home" class="btn btn-link">ホーム</a></li>
			        <li><a href="newMessage" class="btn btn-link">新規投稿</a></li>
			        <li><a href="management" class="btn btn-link">ユーザー管理</a></li>
			        <li><a href="logout" class="btn btn-link">ログアウト</a></li>
			   </ol>
    		</div>
            <form action="signup" method="post">
            	<br /> <label for="loginId">ログインID</label> <input name="loginId" value="${user.loginId}" />
            	※半角英数字[azAZ0*9]で6文字以上20文字以下
            	<br /> <label for="password">パスワード</label> <input name="password" type="password" value="${user.password}" />
            	※記号を含む全ての半角文字で6文字以上20文字以下
            	<br /> <label for="passwordCheck">パスワード（確認用）</label> <input name="passwordCheck" type="password" value="${user.passwordCheck}" />
                <br /> <label for="name">名称</label> <input name="name" value="${user.name}" />
                ※10文字以下

                <br /> <label for="officeId">支店</label>
                <select name="officeId">
                <option value=1>本社</option>
                <option value=2>支店A</option>
                <option value=3>支店B</option>
                <option value=4>支店C</option>
                </select>

                <br /> <label for="positionId">部署・役職</label>
                <select name="positionId">
                <option value=1>総務人事担当者</option>
                <option value=2>情報管理担当者</option>
                <option value=3>支店長</option>
                <option value=4>社員</option>
                </select>

                <br /> <label for="status">アカウントステータス</label>
                <select name="status">
                <option value=1>有効</option>
                <option value=2>無効</option>
                </select>
                <br />
                <br /> <input type="submit" class="btn btn-default" value="登録" /> <br /> <a href="management">戻る</a>
            </form>
        </div>
    </body>
</html>